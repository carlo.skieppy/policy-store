namespace PolicyApi.Models
{
    public class PolicyStoreDatabaseSettings : IPolicyStoreDatabaseSettings
    {
        public string PoliciesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IPolicyStoreDatabaseSettings
    {
        string PoliciesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}